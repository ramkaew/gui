import java.awt.*;
import javax.swing.*;
import java.io.*;
import java.util.*; 
import java.awt.event.*;


public class ScreenPlay extends JFrame{
    protected JPanel panelWest, panelSouth,panelCenter,noviceCenterPN,monsterCenterPN,nameMonsterPN,panelEath;
    protected JPanel panelBagBT,panelAttackBT,panelLeaveBT,panelScoreKill,panelUpdateBT,panelHP;
    protected JLabel labelStatus, statusName,statusClass,statusLevel,statusDamage, statusExp,space,monsterNameLB;
    protected JLabel statusMonster,statusNameMons,statusDamageMons,statusMonsHP,statusHP,scoreKillLB,massage,labelHP;
    protected JPanel skill1PN;
    protected Font font,fontStatus,fontButton,fontMons;
    protected String noviceName,noviceClass,nameMoster;
    protected int level , damage ,hp, exp ,scoreKill,damageMons,monsHP;
    protected JButton bagButton, attackButton,leaveButton,updateBT,skill1Button;
    protected Image charNovice,scaleNovice,scaleMonster,scaleMon1,scaleMon2,scaleMon3,scaleMon4;
    protected ImageIcon noviceIcon,monsterIcon,mon1IN,mon2IN,mon3IN,mon4IN;

    public void setNameMonster(String nameMons){
        nameMoster = nameMons;
    }
    public String getNameMonster(){
        return nameMoster ;
    }
    public void setDamageMonster(int damage){
        damageMons = damage ;
    }
    public int getDamageMonster(){
        return damageMons ;
    }
    public void setMonsterHP(int hpMon){
        monsHP = hpMon ;
    }
    public int getMonsterHP(){
        return monsHP;
    }
    public void setScoreKill(int score){
        scoreKill +=score;
    } 
    public int getScoreKill(){
        return scoreKill;
    }

   

    public ScreenPlay(String name){
        scoreKill = 0 ;
        noviceName = name ;
        hp = 500 ;
        setLayout(new BorderLayout());
        setTitle("Fighting Monsters");
        setSize(1200,720);
        setResizable(false);                             // Fix size frame
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        setBackground(Color.WHITE);

        noviceCenterPN = new JPanel(new GridLayout(2,1));
        monsterCenterPN = new JPanel(new GridLayout(2,1));
        panelBagBT = new JPanel();
        panelAttackBT = new JPanel();
        panelLeaveBT = new JPanel();
        panelCenter = new JPanel(new BorderLayout());
        panelSouth = new JPanel(new GridLayout(1,6));
        bagButton = new JButton(" BAG ");
        bagButton.setPreferredSize(new Dimension(150, 45));  
        font = new Font("Kanyanut uni",Font.BOLD,25);
        fontStatus = new Font("Kanyanut uni", Font.BOLD, 17);
        fontButton = new Font("Kanyanut uni",Font.BOLD,22);
        fontMons = new Font("Kanyanut uni",Font.BOLD,30);

        statusMonster = new JLabel("STATUS Monster");
        statusMonster.setFont(font);
        statusMonster.setForeground(Color.RED);
        statusMonster.setHorizontalAlignment(SwingConstants.CENTER);

        noviceIcon = new ImageIcon("CharNovice.jpg");
        monsterIcon = new ImageIcon("Mons.jpg");
        mon1IN = new ImageIcon("GreatGirros.jpg");
        mon2IN = new ImageIcon("Paolumu.jpg");
        mon3IN = new ImageIcon("Base.jpg");
        mon4IN = new ImageIcon("Teostra.jpg");

        
        scoreKillLB = new JLabel("Score KIll : "+getScoreKill());
        scoreKillLB.setFont(fontMons);
        scoreKillLB.setHorizontalAlignment(SwingConstants.CENTER);
        labelHP = new JLabel("HP : "+hp);
        labelHP.setFont(fontMons);
        labelHP.setHorizontalAlignment(SwingConstants.CENTER);
        panelHP = new JPanel();
        panelHP.add(labelHP);

        panelScoreKill = new JPanel(new GridLayout(2,1));
        panelScoreKill.add(scoreKillLB);
        panelScoreKill.add(labelHP);
    

        damageMons = randomDamageMonster(getScoreKill()) ;
        statusDamageMons = new JLabel("Damage : "+damageMons);
        statusDamageMons.setFont(fontStatus);
        statusDamageMons.setHorizontalAlignment(SwingConstants.CENTER);

        monsHP = randomMonsterHP(getScoreKill());
        statusMonsHP = new JLabel("Health point(HP): "+monsHP);
        statusMonsHP.setFont(fontStatus);
        statusMonsHP.setHorizontalAlignment(SwingConstants.CENTER);

        nameMoster = randomMonster();
        monsterNameLB = new JLabel("HP : "+monsHP);
        monsterNameLB.setHorizontalAlignment(SwingConstants.CENTER);
        monsterNameLB.setVerticalAlignment(SwingConstants.CENTER);
        monsterNameLB.setFont(fontMons);
        nameMonsterPN = new JPanel();
        nameMonsterPN.add(monsterNameLB);
        
    

		scaleNovice = scaleImage(noviceIcon.getImage(), 200, 320);
        noviceIcon = new ImageIcon(scaleNovice);
        scaleMonster = scaleImage(monsterIcon.getImage(), 200, 420);
        monsterIcon = new ImageIcon(scaleMonster);

        statusNameMons = new JLabel("Name : "+nameMoster);
        statusNameMons.setFont(fontStatus);
        statusNameMons.setHorizontalAlignment(SwingConstants.CENTER);
        statusNameMons.setVerticalAlignment(SwingConstants.CENTER);

        
        

        
        statusHP = new JLabel("Health point(HP):"+hp);
        statusHP.setFont(fontStatus);
        statusHP.setHorizontalAlignment(SwingConstants.CENTER);
        attackButton = new JButton("ATTACK");
        attackButton.setFont(fontButton);
        attackButton.setPreferredSize(new Dimension(150, 45)); 
        bagButton.setFont(fontButton);
        leaveButton = new JButton("LEAVE");
        leaveButton.setToolTipText("Random monsters !!!");
        leaveButton.setFont(fontButton);
        leaveButton.setForeground(Color.RED);
        leaveButton.setPreferredSize(new Dimension(150, 45)); 
        updateBT = new JButton("Exit game");
        updateBT.setFont(fontButton);
        updateBT.setPreferredSize(new Dimension(150, 45)); 
        updateBT.addActionListener(new ButtonListener());

       
        panelWest = new JPanel(new GridLayout(7,1));
        monsterCenterPN.add(nameMonsterPN);
        monsterCenterPN.add(new JLabel(monsterIcon));

        
        labelStatus = new JLabel("STATUS Novice");
        labelStatus.setFont(font);
        labelStatus.setForeground(Color.BLUE);
        labelStatus.setHorizontalAlignment(SwingConstants.CENTER);
        
        statusName = new JLabel("Novice name : "+noviceName);
        statusName.setFont(fontStatus);
        statusName.setHorizontalAlignment(SwingConstants.CENTER);

        level = 1 ;
        statusLevel = new JLabel("My Level :"+level);
        statusLevel.setFont(fontStatus);
        statusLevel.setHorizontalAlignment(SwingConstants.CENTER);
        noviceClass ="No class";
        statusClass = new JLabel("Novice class :"+noviceClass);
        statusClass.setFont(fontStatus);
        statusClass.setHorizontalAlignment(SwingConstants.CENTER);        
        damage = 10 ;
        statusDamage = new JLabel("My Damage :"+damage);
        statusDamage.setFont(fontStatus);
        statusDamage.setHorizontalAlignment(SwingConstants.CENTER);
        exp = 0;
        statusExp = new JLabel("Experience point( EXP ) :"+exp+"Point");
        statusExp.setFont(fontStatus);
        statusExp.setHorizontalAlignment(SwingConstants.CENTER);

        skill1Button = new JButton("Ultimate");
        skill1Button.setPreferredSize(new Dimension(150,45));
        skill1Button.setFont(fontButton);

        skill1PN = new JPanel();
        skill1PN.add(skill1Button);
        skill1Button.addActionListener(new ButtonListener());

        bagButton.addActionListener(new ButtonListener());
        attackButton.addActionListener(new ButtonListener());
        leaveButton.addActionListener(new ButtonListener());
        noviceCenterPN.add(panelScoreKill);
       
        noviceCenterPN.add(new JLabel(noviceIcon));

        
        
        
        panelCenter.add(noviceCenterPN, BorderLayout.WEST);
        panelCenter.add(monsterCenterPN,BorderLayout.EAST);

        panelWest.add(labelStatus);
        panelWest.add(statusName);
        panelWest.add(statusHP);
        panelWest.add(statusClass);
        panelWest.add(statusLevel);
        panelWest.add(statusExp);
        panelWest.add(statusDamage);

        panelUpdateBT = new JPanel();
        panelEath = new JPanel(new GridLayout(7,1));
        panelEath.add(statusMonster);
        panelEath.add(statusNameMons);
        panelEath.add(statusMonsHP);
        panelEath.add(statusDamageMons);

        space = new JLabel(" ");
        panelUpdateBT.add(updateBT);
        panelBagBT.add(bagButton);
        panelAttackBT.add(attackButton);
        panelLeaveBT.add(leaveButton);
        
        panelSouth.add(panelUpdateBT);
        panelSouth.add(space);
        panelSouth.add(panelBagBT);
        panelSouth.add(panelAttackBT);
        panelSouth.add(skill1PN);
        panelSouth.add(panelLeaveBT);
        add(panelWest,BorderLayout.WEST);
        add(panelSouth,BorderLayout.SOUTH);
        add(panelCenter,BorderLayout.CENTER);
        add(panelEath,BorderLayout.EAST);
        
    }
   
    
    
    public Image scaleImage(Image image, int w, int h) {

        Image scaled = image.getScaledInstance(w, h, Image.SCALE_SMOOTH);
    
        return scaled;
    }

    public String randomMonster(){
        String nameMonster ;
        Random rnd = new Random();
        int selectKey = rnd.nextInt(4);

        if(selectKey == 0 ){
            return nameMonster = "Great Girros";
        }else if(selectKey == 1 ){
            return nameMonster = "Paolumu";
        }else if(selectKey == 2){
            return nameMonster = "Baselgeuse";
        }else if(selectKey == 3){
            return nameMonster = "TeostraK" ;
        }else
        return "";

    }
    
    public int randomDamageMonster(int scoreKill){
        int score = scoreKill ;
        Random rnd = new Random();
        int damage = rnd.nextInt(10);
        return  (damage*score)+10 ;
   }
   public int randomMonsterHP(int scoreKill){
    int score = scoreKill ;
    Random rnd = new Random();
    int monHP = rnd.nextInt(100);
    return  (monHP*score)+100 ;
}
private class ButtonListener implements ActionListener{
    public void actionPerformed(ActionEvent e){
        if(e.getSource()== bagButton){
            JOptionPane.showMessageDialog(null,"No items !!!");
        }else if(e.getSource()==updateBT){
            System.exit(0);
        }else if(e.getSource()== attackButton){
            monsHP = monsHP - damage;
            monsterNameLB.setText("HP : "+monsHP);
            if(monsHP > 0){
                hp = hp - damageMons ;
                statusHP.setText("Health point(HP): "+hp);
                labelHP.setText("HP : "+hp);
                if(hp <= 0){
                    JOptionPane.showMessageDialog(null,"You dead !!!!");
                    System.exit(0);
                }
            }else if(monsHP <= 0){
                JOptionPane.showMessageDialog(null,"You Win !!!");
                setScoreKill(1) ;
                scoreKillLB.setText("Score KIll : "+getScoreKill());
                exp +=3 ;
                statusExp.setText("Experience point( EXP ) :"+exp+"Point");
                if(exp >=10){
                    exp = 0 ;
                statusExp.setText("Experience point( EXP ) :"+exp+"Point");
                hp +=100 ;
                statusHP.setText("Health point(HP): "+hp);
                labelHP.setText("HP : "+hp);
                level++;
                statusLevel.setText("My Level :"+level);
                damage += 5;
                statusDamage.setText("My Damage :"+damage);
                
                }

                setNameMonster(randomMonster());
            monsterNameLB.setText("HP : "+monsHP);
            statusNameMons.setText(getNameMonster());
            setDamageMonster(randomDamageMonster(getScoreKill()));
            statusDamageMons.setText("Damage : "+getDamageMonster());
            setMonsterHP(randomMonsterHP(getScoreKill()));
            statusMonsHP.setText("Health point(HP): "+getMonsterHP());

                }


        }else if(e.getSource() == skill1Button){
            monsHP = monsHP - 30;
            monsterNameLB.setText("HP : "+monsHP);
            if(monsHP > 0){
                hp = hp - damageMons ;
                statusHP.setText("Health point(HP): "+hp);
                labelHP.setText("HP : "+hp);
                if(hp <= 0){
                    JOptionPane.showMessageDialog(null,"You dead !!!!");
                    System.exit(0);
                }
            }else if(monsHP <= 0){
                JOptionPane.showMessageDialog(null,"You Win !!!");
                setScoreKill(1) ;
                scoreKillLB.setText("Score KIll : "+getScoreKill());
                exp +=5 ;
                statusExp.setText("Experience point( EXP ) :"+exp+"Point");
                if(exp >=15){
                    exp = 0 ;
                statusExp.setText("Experience point( EXP ) :"+exp+"Point");
                level++;
                hp +=100 ;
                statusHP.setText("Health point(HP): "+hp);
                labelHP.setText("HP : "+hp);
                statusLevel.setText("My Level :"+level);
                damage += 5;
                statusDamage.setText("My Damage :"+damage);
                
                }

                setNameMonster(randomMonster());
            monsterNameLB.setText("HP : "+monsHP);
            statusNameMons.setText(getNameMonster());
            setDamageMonster(randomDamageMonster(getScoreKill()));
            statusDamageMons.setText("Damage : "+getDamageMonster());
            setMonsterHP(randomMonsterHP(getScoreKill()));
            statusMonsHP.setText("Health point(HP): "+getMonsterHP());

                }

        }else if(e.getSource()==leaveButton){
            setNameMonster(randomMonster());
            monsterNameLB.setText("HP : "+monsHP);
            statusNameMons.setText(getNameMonster());
            setDamageMonster(randomDamageMonster(getScoreKill()));
            statusDamageMons.setText("Damage : "+getDamageMonster());
            setMonsterHP(randomMonsterHP(getScoreKill()));
            statusMonsHP.setText("Health point(HP): "+getMonsterHP());  // update and have update image

        }
    }    
}


}