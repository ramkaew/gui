import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;

public class ScreenHome extends JFrame{
 
    private JPanel panelWest,panelCenter, panelNorth , panelWelcome, panelName, panelButtonNorth, panelButtonStart;
    private JLabel welcome, name, newLabel , jLabel, logoLabel ,space1 ;
    private ImageIcon icon,logo ,scaledIcon, scaleLogoImgIcon ;
    private Image image, scaled ,scaleLogo;
    private JButton startButton, contectButton ;
    private JTextField nameTextField ;
    private Font font, fontBT ;


    public ScreenHome(){
        setLayout(new BorderLayout());
        
        contectButton = new JButton("Contect Us");
        contectButton.setPreferredSize(new Dimension(200, 75));     //Set size button
        font = new Font("Kanyanut uni",Font.BOLD,45);
        fontBT = new Font("Kanyanuy uni", Font.BOLD, 100);
        contectButton.setFont(font);
        contectButton.setForeground(Color.BLUE);
        contectButton.addActionListener(new ButtonListener());


        
       
        icon = new ImageIcon("CharNovice.jpg"); 
        logo = new ImageIcon("Logo.jpg");

        scaled = scaleImage(icon.getImage(), 350,520);
        scaledIcon = new ImageIcon(scaled);

        scaleLogo = scaleImage(logo.getImage(),300,100);
        scaleLogoImgIcon = new ImageIcon(scaleLogo);

        startButton = new JButton("     START",scaleLogoImgIcon);
        newLabel = new JLabel(scaledIcon);
        logoLabel = new JLabel(scaleLogoImgIcon);
        space1 = new JLabel(" ");
     
        startButton.setFont(fontBT);
        startButton.setForeground(Color.RED);
        startButton.addActionListener(new ButtonListener());

        panelWest = new JPanel();
        panelCenter = new JPanel();
        panelNorth = new JPanel( new GridLayout(1,3));
        panelWelcome = new JPanel();
        panelName = new JPanel();
        panelButtonNorth = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        panelButtonStart = new JPanel();

        welcome = new JLabel(" Welcome  to  game  \" Fighting Monsters \" ");
        welcome.setFont(font);
        
        name = new JLabel("What your name : ");
        name.setFont(font);
        
        startButton.setFont(font);
        nameTextField = new JTextField(20);
        nameTextField.setFont(font);
        nameTextField.setHorizontalAlignment(SwingConstants.CENTER);
    

        panelButtonStart.add(startButton);
        panelButtonNorth.add(contectButton);
        panelWelcome.add(welcome);
        panelName.add(name);
        panelName.add(nameTextField);
        panelCenter.setLayout(new GridLayout(4,1,0,20));
        panelCenter.add(panelWelcome);
        panelCenter.add(panelName);
        panelCenter.add(panelButtonStart);
        
        panelWest.add(newLabel);

        panelNorth.add(logoLabel);
        panelNorth.add(space1);
        panelNorth.add(panelButtonNorth);
        add(panelWest,BorderLayout.WEST);
        add(panelCenter,BorderLayout.CENTER);
        add(panelNorth,BorderLayout.NORTH);
        setTitle("Fighting Monsters");
        setSize(1200,720);
        setResizable(false);                             // Fix size frame
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }

    public Image scaleImage(Image image, int w, int h) {

        Image scaled = image.getScaledInstance(w, h, Image.SCALE_SMOOTH);
    
        return scaled;
    }
   

    private class ButtonListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            if(e.getSource()== contectButton){
                JOptionPane.showMessageDialog(null,"Tel. : 0808696541 \nE-mail : akekaphol101@gmail.com");
            }else if(e.getSource()== startButton){
                
              load();
                
            }
        }
    } 

    public void load(){
       String name;
       name = this.getName();
        ScreenPlay play = new ScreenPlay( name );
        this.dispose();

    }


        // get name
    public String getName(){
        return nameTextField.getText(); 
    }


}

